package org.mozhu.learning.spi.core.json;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JSONTest {

    User user = User.builder()
            .name("zhangsan")
            .age(18)
            .build();

    String userJsonString = "{\"name\":\"zhangsan\",\"age\":18}";

    @Test
    public void toJson() {
        String actualJsonString = JSON.toJson(user);
        assertEquals(userJsonString, actualJsonString);
    }

    @Test
    public void toJavaObject() {
        User actualUser = JSON.toJavaObject(userJsonString, User.class);
        assertEquals(user, actualUser);
    }

}